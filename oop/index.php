<?php 
	require ('animal.php');
	require ('frog.php');
	require ('ape.php');
	
	//release 0
	$sheep = new animal("shaun");
	echo "Name : ".$sheep->get_name()."<br>"; 
	echo "Legs : ".$sheep->get_legs()."<br>"; 
	echo "Cold blooded : ".$sheep->get_cold_blooded()."<br><br>"; 
	
	//release 1
	$sungokong = new Ape("kera sakti");
	echo "Name : ".$sungokong->get_name()."<br>"; 
	echo "Legs : ".$sungokong->get_legs()."<br>"; 
	echo "Cold blooded : ".$sungokong->get_cold_blooded()."<br>"; 
	$sungokong->yell();
	echo "<br><br>";

	$kodok = new frog("buduk");
	echo "Name : ".$kodok->get_name()."<br>"; 
	echo "Legs : ".$kodok->get_legs()."<br>"; 
	echo "Cold blooded : ".$kodok->get_cold_blooded()."<br>"; 
	$kodok->jump() ;
	
 ?>